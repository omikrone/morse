﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Morseova_abeceda
{
    class Program
    {
        static void Main(string[] args)
        {
            //Vrácení na začátek programu po úspěšném provedení překladu
        znovu:
            Console.WriteLine("1. Převod z textu na morseovku");
            Console.WriteLine("2. Převod z morseovky na text");
            Console.WriteLine("0. Zavřít");
            //Přečte se vstup a posoudí o výběru uživatele, jak dále chce pokračovat
            string vstup = Console.ReadLine();
            if (vstup == "1"){
                
                Console.WriteLine("Zadejte text pro převod z textu na morseovku:");
                string input = "";
                string output = "";
                input = Console.ReadLine();
                
                //Nadefinování polí písmena a písmena v morseovce kde je jsou uloženy všechna písmena kromě ch a všechna čísla
                char[] letters = { ' ', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
                string[] morseLetters = { " ", ".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--..", ".----", "..---", "...--", "....-", ".....", "-....", "--...", "---..", "----.", "-----" };

                //Cyklus se provádí dokud není "i" větší než vstup uživatele (ověřuje se pro každý znak)
                for (int i = 0; i < input.Length; i++)
                {
                    //Cyklus prochází všechny znaky v poli
                    for (short j = 0; j < 37; j++)
                    {
                        //Ověření jestli se znak nachází na dané pozici v poli pokud ano tak se znak připíše do proměnné "output" a za to znak "/". Pokud se na dané pozici nenachází pokračuje v procházení zanků v poli
                        if (input[i] == letters[j])
                        {
                            output += morseLetters[j];
                            output += "/";
                            break;
                        }
                    }
                }
                //Odřádkování a následné vypsaní překládaného textu
                Console.WriteLine("");
                Console.WriteLine("Morseovka:");
                Console.WriteLine(output);
                Console.WriteLine("");
                //Vrátí se na začatek programu a umožní uživateli vybrat další krok
                goto znovu;

            }else if (vstup == "2"){

                Console.WriteLine("Zadejte text pro převod z morseovky na text:");
                string input = Console.ReadLine();
                string slovo = "";
                //Nadefinování polí písmena a písmena v morseovce kde je jsou uloženy všechna písmena kromě ch a všechna čísla
                char[] letters = { ' ', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
                string[] morseLetters = { " ", ".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--..", ".----", "..---", "...--", "....-", ".....", "-....", "--...", "---..", "----.", "-----" };
                //Převezme vstup od uživatele a díky fuknci Split rozdělí celý řetězec na části před znakem "/"
                string[] words = input.Split('/');

                //Cyklus se provádí dokud není "i" větší než vstup uživatele (ověřuje se pro každý znak)
                for (int i = 0; i < words.Length; i++)
                {
                    //Ověření jestli se znak nachází na dané pozici v poli pokud ano tak se znak připíše do proměnné "output" a za to znak "/". Pokud se na dané pozici nenachází pokračuje v procházení zanků v poli
                    for (int j = 0; j < 37; j++)
                    {
                            if (words[i] == morseLetters[j])
                            {
                                slovo += letters[j];
                                break;
                            }
                    }
                }
                //Odřádkování a následné vypsaní překládaného textu
                Console.WriteLine("");
                Console.WriteLine("Text:");
                Console.WriteLine(slovo);
                Console.WriteLine("");
                //Vrátí se na začatek programu a umožní uživateli vybrat další krok
                goto znovu;

            }else if (vstup == "0")
            {
                //Vyskočí z podmínky a ukončí program
                Environment.Exit(0);
            }
            Console.ReadKey();
        }
    }
}
